function inverte(x){
    let i,aux=0;
    let n = x.length;
    for (i=0; i<x.length/2; i++){
      aux = x[i];
      x[i] = x[n-1];
      x[n-1] = aux;
      n = n-1;
    
    }
    return x;
}

let a = [1,2,3,4,5,6,7,8,9,10]
b = inverte(a);
console.log(b);
// b -> [3,2,1]
